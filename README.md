## Installation

- clone project.

```bash
git clone git@bitbucket.org:galinoux/symfony4.git
```

- go to symfony4/web

- launch a composer install

```bash
composer install
```

- edit symfony4/web/.env at line 27 with your own database acess

- create database with :

```bash
bin/console  doctrine:database:create 
```

- create database table : 

````bash
bin/console  doctrine:schema:update  
````

- Load fixture: 

````bash
bin/console doctrine:fixtures:load  
````

- Launch the app with 

````bash
bin/console server:start 
````

admin access : 

login : egf@test.fr

pwd : diskotest


<?php

namespace App\Controller\Back;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/back/users", name="back_user_index")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(User::class);

        $users = $repository->findAll();

        return $this->render('back/user/index.html.twig', [
            'controller_name' => 'UserController',
            'users' => $users
        ]);

    }

    /**
     * @param Request $request
     * @param User $user
     *
     * @Route("/back/user/edit/{id}", name="back_user_edit")
     */
    public function edit(Request $request, User $user, $id, UserPasswordEncoderInterface $passwordEncoder)
    {
        //$this->denyAccessUnlessGranted('edit', $backUser);

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(User::class)->find($id);
        $form = $this->createForm(RegistrationFormType::class, $product);

        $form->handleRequest($request);

        if ('POST' == $request->getMethod()) {

            $userData = $form->getData();

            if ($form->isSubmitted() && $form->isValid()) {

                /** @var User $user */
                $user = $form->getData();

                $user->setEmail($userData->getEmail());
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                return $this->render('back/user/edit.html.twig', [
                    'message' => [
                        'title' => 'sucess',
                        'description' => 'user updated',
                    ],
                    'form' => $form->createView(),
                    'user' => $user,
                ]);
            }

            return $this->render('back/user/edit.html.twig', [
                'message' => [
                    'title' => 'error',
                    'description' => 'user not updated',
                ],
                'form' => $form->createView(),
                'user' => $user,
            ]);
        }

        return $this->render('back/user/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @param Request $request
     * @param User $user
     *
     * @Route("/back/user/delete/{id}", name="back_user_delete")
     */
    public function delete(Request $request, User $user, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('back_user_index', ['message' => [
            'title' => 'Success',
            'description' => 'user deleted',
        ]]);
    }
}

<?php

namespace App\Controller\Back;

use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Templating\Helper\FormHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @property  formHelper
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/back/products", name="back_product_index")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Product::class);

        $products = $repository->findAll();

        return $this->render('back/product/index.html.twig', [
            'controller_name' => 'ProductController',
            'products' => $products
        ]);
    }


    /**
     * @param Request $request
     *
     * @Route("/back/product/create", name="back_product_create")
     *
     * @throws \Exception
     */
    public function create(Request $request)
    {

        // just setup a fresh $task object (remove the dummy data)
        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ('POST' == $request->getMethod()) {
            if ($form->isSubmitted() && $form->isValid()) {
                $product = $form->getData();
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($product);
                $entityManager->flush();

                return $this->render('back/product/create.html.twig', [
                    'message' => [
                        'title' => 'sucess',
                        'description' => 'product created',
                    ],
                    'controller_name' => 'ProductController',
                    'form' => $form->createView(),
                ]);

            }

            return $this->render('back/product/create.html.twig', [
                'message' => [
                    'title' => 'error',
                    'description' => 'product not created',
                ],
                'controller_name' => 'ProductController',
                'form' => $form->createView(),
            ]);
        }
        return $this->render('back/product/create.html.twig', [
            'controller_name' => 'ProductController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Product $product
     *
     * @Route("/back/product/edit/{id}", name="back_product_edit")
     */
    public function edit(Request $request, Product $product, $id)
    {
        //$this->denyAccessUnlessGranted('edit', $backUser);

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if ('POST' == $request->getMethod()) {

            if ($form->isSubmitted() && $form->isValid()) {

                /** @var Product $product */
                $product = $form->getData();

                $em = $this->getDoctrine()->getManager();
                $em->persist($product);
                $em->flush();

                return $this->render('back/product/edit.html.twig', [
                    'message' => [
                        'title' => 'sucess',
                        'description' => 'product updated',
                    ],
                    'form' => $form->createView(),
                    'product' => $product,
                ]);
            }

            return $this->render('back/product/edit.html.twig', [
                'message' => [
                    'title' => 'error',
                    'description' => 'product not updated',
                ],
                'form' => $form->createView(),
                'product' => $product,
            ]);
        }

        return $this->render('back/product/edit.html.twig', [
            'form' => $form->createView(),
            'product' => $product,
        ]);
    }

    /**
     * @param Request $request
     * @param Product $product
     *
     * @Route("/back/product/delete/{id}", name="back_product_delete")
     */
    public function delete(Request $request, Product $product, $id)
    {
        //$this->denyAccessUnlessGranted('edit', $backUser);

        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);

        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('back_product_index', ['message' => [
            'title' => 'Success',
            'description' => 'product deleted',
        ]]);
    }

}

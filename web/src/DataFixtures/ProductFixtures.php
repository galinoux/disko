<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i < 10; $i++) {
            $product = new Product();
            $product->setName('Produit '.$i);
            $product->setPrice(rand(0, 1000));
            $product->setDescription(str_shuffle('consectetur adipiscing elit. Maecena ultr'));
            $manager->persist($product);
        }

        $manager->flush();
    }
}
